/**
 * 测试用的actor
 */
package org.beykery.bakka.test;

import akka.actor.ActorRef;
import org.beykery.bakka.Bakka;
import org.beykery.bakka.BakkaRequest;
import org.beykery.bakka.BaseActor;
import org.beykery.bakka.Bootstrap;

/**
 *
 * @author beykery
 */
@Bakka(service = "Fronted",slaves = {"Backend"})
public class Fronted extends BaseActor
{

  public Fronted(Bootstrap bs)
  {
    super(bs);
  }
  @BakkaRequest
  public void hi(HI hi)
  {
    System.out.println(hi+" fronted, ......");
    ActorRef sender=this.services.get("Backend").get(0);
    System.out.println(sender.path().address());
    sender.tell(hi, self());
  }

}
